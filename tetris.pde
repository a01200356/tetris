char tecla = 'x'; //Tecla actualmente presionada. 'x' representa que no hay tecla presionada.
int mapa[][]; //Cada posición de la matriz indica el color que contiene.
Tetromino actual;
int velocidad;
int puntaje;
int lineas;
boolean pausa;
boolean finJuego;

void setup()
{
	size(10*20,20*20); //Cada cuadrito es de 20px^2.
	mapa = new int[14][26]; //Incluye bordes y un espacio superior no visibles (para detección de colisiones).
	inicializarMapa();
	inicializarTetromino();
	velocidad = 0;
	puntaje = 0;
	lineas = 0;
	pausa = false;
	finJuego = false;
}

private void inicializarMapa()
{
	//Bordes inferior y laterales del mapa.
	//No son visibles pero sirven para detectar colisiones.
	for(int j = 0; j < 25; j++){
		mapa[0][j] = #FFFFFF;
		mapa[1][j] = #FFFFFF;
		mapa[12][j] = #FFFFFF;
		mapa[13][j] = #FFFFFF;
	}
	for(int i = 0; i < 14; i++){
		mapa[i][24] = #FFFFFF;
		mapa[i][25] = #FFFFFF;
	}
	//El resto del mapa está vacío por ahora.
	for(int i = 2; i < 12; i++){
		for(int j = 0; j < 24; j++){
			mapa[i][j] = 0;
		}
	}
}

private void inicializarTetromino()
{
	switch((int)random(7))
	{
		case 0:
			actual = new Tetromino('I',(int)random(4));
		break;
		case 1:
			actual = new Tetromino('J',(int)random(4));
		break;
		case 2:
			actual = new Tetromino('L',(int)random(4));
		break;
		case 3:
			actual = new Tetromino('O',(int)random(4));
		break;
		case 4:
			actual = new Tetromino('S',(int)random(4));
		break;
		case 5:
			actual = new Tetromino('T',(int)random(4));
		break;
		case 6:
			actual = new Tetromino('Z',(int)random(4));
		break;
	}
	//El juego se pierde cuando un nuevo tetrómino ya no cabe en el mapa sin colisionar.
	if(colision()){
		finJuego = true;
		println("¡PERDISTE! Líneas: " + lineas + " Puntaje: " + puntaje);
	}
}

void draw()
{
	if(!pausa && !finJuego){
		switch(tecla){
			case 'w':
				actual.girarDerecha();
				if(colision()){
					actual.girarIzquierda();
				}
			break;
			case 'a':
				actual.moverIzquierda();
				if(colision()){
					actual.moverDerecha();
				}
			break;
			case 's':
				bajarTetromino();
			break;
			case 'd':
				actual.moverDerecha();
				if(colision()){
					actual.moverIzquierda();
				}
			break;
			case ' ':
				soltarTetromino();
			break;
		}
		tecla = 'x';
		
		//Dibuja las partes visibles del mapa actual.
		for(int i = 2; i < 12; i++){
			for(int j = 4; j < 24; j++){
				fill(mapa[i][j]);
				rect((i-2)*20,(j-4)*20,20,20);
			}
		}
		//Dibuja el tetrómino actual.
		fill(actual.Color());
		for(int i = 0; i < 4; i++){
			for(int j = 0; j < 4; j++){
				if(actual.ocupado(i,j)){
						rect((actual.PosX()+i-2)*20, (actual.PosY()+j-4)*20, 20, 20);
				}
			}
		}
		//El tetrómino desciende a una frecuencia proporcional a la velocidad.
		//velocidad siempre es menor a 20.
		if(frameCount % (20 - velocidad) == 0){
			bajarTetromino();
		}
	}
}

//El tetrómino baja una sola línea.
private void bajarTetromino()
{
	actual.bajar();
	if(colision()){
		atorarTetromino();
		puntaje += 10*(velocidad+1);
		println(puntaje);
		revisarLineas();
		inicializarTetromino();
	}
}

//El tetrómino baja hasta que colisione.
private void soltarTetromino()
{
	while(!colision()){
		actual.bajar();
	}
	atorarTetromino();
	puntaje += 10*(velocidad+1);
	println(puntaje);
	revisarLineas();
	inicializarTetromino();
}

private boolean colision()
{
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 4; j++){
			if(actual.ocupado(i, j) && mapa[i+actual.PosX()][j+actual.PosY()] != 0){
				return true;
			}
		}
	}
	return false;
}

//Imprime en el mapa la posición del tetrómino.
//Este modo solo se manda a llamar cuando el tetrómino ha colisionado.
private void atorarTetromino()
{
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 4; j++){
			if(actual.ocupado(i, j)){
				mapa[i+actual.PosX()][j+actual.PosY()-1] = actual.Color();
			}
		}
	}
}

//Verifica si alguna línea se ha completado (no tiene ceros).
private void revisarLineas()
{
	int ceros;
	for(int j = 23; j >= 0; j--){
		ceros = 0;
		for(int i = 2; i < 12; i++){
			if(mapa[i][j] == 0){
				ceros++;
			}
		}
		if(ceros == 0){
			for(int J = j; J > 0; J--){
				for(int i = 2; i < 12; i++){
					mapa[i][J] = mapa[i][J-1];
				}
			}
			for(int i = 2; i < 12; i++){
				mapa[i][0] = 0;
			}
			j++;
			puntaje += 100*(velocidad+1);
			println(puntaje);
			lineas++;
			//La velocidad máxima es de 18.
			//Si esto se cambia también se deberá cambiar la llamada a bajarTetromino al final del método draw.
			if(lineas % 10 == 0 && velocidad < 18){
				velocidad+=3;
			}
		}
	}
}

void keyPressed()
{
	if(key == 'w' || key == 'W' || keyCode == UP){
		tecla = 'w';
	}
	else if(key == 'a' || key == 'A' || keyCode == LEFT){
		tecla = 'a';
	}
	else if(key == 's' || key == 'S' || keyCode == DOWN){
		tecla = 's';
	}
	else if(key == 'd' || key == 'D' || keyCode == RIGHT){
		tecla = 'd';
	}
	else if(key == ' '){
		tecla = ' ';
	}
	else if(key == 'p' || key == 'P'){
		pausa = !pausa;
	}
}
void keyReleased()
{
	tecla = 'x';
}
