public class Tetromino
{
	char tipo; //Identificador de cada tetrómino según su forma (I, J, L, S, Z, O, T).
	int posActual;
	int esquinaX;
	int esquinaY;
	int colorForma;
	int forma[][];
	public Tetromino(char tipo, int posInicial)
	{
		this.tipo = tipo;
		if(posInicial > 3 || posInicial < 0){
			posActual = 0;
		}else{
			posActual = posInicial;
		}
		esquinaX = 5;
		esquinaY = 0;
		forma = new int[4][4];
		resetearForma();
		rellenarForma();
	}
	
	public void girarDerecha()
	{
		if(posActual == 3){
			posActual = 0;
		}else{
			posActual++;
		}
		resetearForma();
		rellenarForma();
	}
	
	public void girarIzquierda()
	{
		if(posActual == 0){
			posActual = 3;
		}else{
			posActual--;
		}
		resetearForma();
		rellenarForma();
	}
	
	public void moverDerecha()
	{
		esquinaX++;
	}
	
	public void moverIzquierda()
	{
		esquinaX--;
	}
	
	public void bajar()
	{
		esquinaY++;
	}
	
	public boolean ocupado(int x, int y)
	{
		if(forma[x][y] != 0){
			return true;
		}else{
			return false;
		}
	}
	
	public int PosX()
	{
		return esquinaX;
	}
	
	public int PosY()
	{
		return esquinaY;
	}
	
	public color Color()
	{
		return colorForma;
	}
	
	private void resetearForma(){
		for(int i = 0; i < 4; i++){
			for(int j = 0; j < 4; j++){
				forma[i][j] = 0;
			}
		}
	}
	
	private void rellenarForma(){
		switch(tipo)
		{
			case 'I':
			colorForma = #00FFFF;
				switch(posActual)
				{
					case 0:
						forma[2][0] = #00FFFF;
						forma[2][1] = #00FFFF;
						forma[2][2] = #00FFFF;
						forma[2][3] = #00FFFF;
					break;
					case 1:
						forma[0][2] = #00FFFF;
						forma[1][2] = #00FFFF;
						forma[2][2] = #00FFFF;
						forma[3][2] = #00FFFF;
					break;
					case 2:
						forma[1][0] = #00FFFF;
						forma[1][1] = #00FFFF;
						forma[1][2] = #00FFFF;
						forma[1][3] = #00FFFF;
					break;
					case 3:
						forma[0][1] = #00FFFF;
						forma[1][1] = #00FFFF;
						forma[2][1] = #00FFFF;
						forma[3][1] = #00FFFF;
					break;
				}
			break;
			case 'J':
			colorForma = #0000FF;
				switch(posActual)
				{
					case 0:
						forma[1][3] = #0000FF;
						forma[2][1] = #0000FF;
						forma[2][2] = #0000FF;
						forma[2][3] = #0000FF;
					break;
					case 1:
						forma[1][1] = #0000FF;
						forma[1][2] = #0000FF;
						forma[2][2] = #0000FF;
						forma[3][2] = #0000FF;
					break;
					case 2:
						forma[2][1] = #0000FF;
						forma[1][1] = #0000FF;
						forma[1][2] = #0000FF;
						forma[1][3] = #0000FF;
					break;
					case 3:
						forma[2][3] = #0000FF;
						forma[0][2] = #0000FF;
						forma[1][2] = #0000FF;
						forma[2][2] = #0000FF;
					break;
				}
			break;
			case 'L':
			colorForma = #FF8000;
				switch(posActual)
				{
					case 0:
						forma[2][3] = #FF8000;
						forma[1][1] = #FF8000;
						forma[1][2] = #FF8000;
						forma[1][3] = #FF8000;
					break;
					case 1:
						forma[1][2] = #FF8000;
						forma[1][1] = #FF8000;
						forma[2][1] = #FF8000;
						forma[3][1] = #FF8000;
					break; 
					case 2:
						forma[1][1] = #FF8000;
						forma[2][1] = #FF8000;
						forma[2][2] = #FF8000;
						forma[2][3] = #FF8000;
					break;
					case 3:
						forma[3][1] = #FF8000;
						forma[1][2] = #FF8000;
						forma[2][2] = #FF8000;
						forma[3][2] = #FF8000;
					break;
				}
			break;
			case 'O':
			colorForma = #FFFF00;
				switch(posActual)
				{
					case 0:
						forma[1][1] = #FFFF00;
						forma[1][2] = #FFFF00;
						forma[2][2] = #FFFF00;
						forma[2][1] = #FFFF00;
					break;
					case 1:
						forma[1][1] = #FFFF00;
						forma[1][2] = #FFFF00;
						forma[2][2] = #FFFF00;
						forma[2][1] = #FFFF00;
					break;
					case 2:
						forma[1][1] = #FFFF00;
						forma[1][2] = #FFFF00;
						forma[2][2] = #FFFF00;
						forma[2][1] = #FFFF00;
					break;
					case 3:
						forma[1][1] = #FFFF00;
						forma[1][2] = #FFFF00;
						forma[2][2] = #FFFF00;
						forma[2][1] = #FFFF00;
					break;
				}
			break;
			case 'S':
			colorForma = #00FF00;
				switch(posActual)
				{
					case 0:
						forma[2][1] = #00FF00;
						forma[3][1] = #00FF00;
						forma[1][2] = #00FF00;
						forma[2][2] = #00FF00;
					break;
					case 1:
						forma[1][0] = #00FF00;
						forma[1][1] = #00FF00;
						forma[2][1] = #00FF00;
						forma[2][2] = #00FF00;
					break;
					case 2:
						forma[2][1] = #00FF00;
						forma[3][1] = #00FF00;
						forma[1][2] = #00FF00;
						forma[2][2] = #00FF00;
					break;		 
					case 3:
						forma[1][0] = #00FF00;
						forma[1][1] = #00FF00;
						forma[2][1] = #00FF00;
						forma[2][2] = #00FF00;
					break;
				}
			break;
			case 'T':
			colorForma = #FF00FF;
				switch(posActual)
				{
					case 0:
						forma[2][3] = #FF00FF;
						forma[1][2] = #FF00FF;
						forma[2][2] = #FF00FF;
						forma[3][2] = #FF00FF;
					break;
					case 1:
						forma[1][2] = #FF00FF;
						forma[2][1] = #FF00FF;
						forma[2][2] = #FF00FF;
						forma[2][3] = #FF00FF;
					break;
					case 2:
						forma[2][1] = #FF00FF;
						forma[1][2] = #FF00FF;
						forma[2][2] = #FF00FF;
						forma[3][2] = #FF00FF;
					break;
					case 3:
						forma[3][2] = #FF00FF;
						forma[2][1] = #FF00FF;
						forma[2][2] = #FF00FF;
						forma[2][3] = #FF00FF;
					break;
				}
			break;
			case 'Z':
			colorForma = #FF0000;
				switch(posActual)
				{
					case 0:
						forma[1][1] = #FF0000;
						forma[2][1] = #FF0000;
						forma[2][2] = #FF0000;
						forma[3][2] = #FF0000;
					break;
					case 1:
						forma[1][1] = #FF0000;
						forma[1][2] = #FF0000;
						forma[2][0] = #FF0000;
						forma[2][1] = #FF0000;
					break;
					case 2:
						forma[1][1] = #FF0000;
						forma[2][1] = #FF0000;
						forma[2][2] = #FF0000;
						forma[3][2] = #FF0000;
					break;
					case 3:
						forma[1][1] = #FF0000;
						forma[1][2] = #FF0000;
						forma[2][0] = #FF0000;
						forma[2][1] = #FF0000;
					break;
				}
			break;
		}
	}
}
